package com.example.springbatchdemo;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;


@SpringBootApplication
public class SpringBatchDemoApplication {

	public static void main(String[] args) {
		System.setProperty("input", "file:"+ new File("C:/drisal/in.csv").getAbsolutePath());
		System.setProperty("output", "file:"+ new File("C:/drisal/out.csv").getAbsolutePath());
		SpringApplication.run(SpringBatchDemoApplication.class, args);
	}
}
